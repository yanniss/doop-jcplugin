package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.VarSymbol;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.doop.jcplugin.util.UsageIdGen;
import org.clyze.persistent.metadata.Printer;
import org.clyze.persistent.model.Position;
import org.clyze.persistent.model.Usage;
import org.clyze.persistent.model.UsageKind;

/**
 * Represents a field usage, i.e. appropriate for all field occurrences except in definitions
 */
public class FieldUsage extends Usage {

    public FieldUsage(JavacFileInfo fInfo, long offset, VarSymbol sym,
                      UsageKind kind, Printer p) {
        this(fInfo.getPosition(offset, sym.getSimpleName().toString()),
                fInfo.getSourceFileName(),
                Field.buildDoopId(p, sym),
                kind);
    }

    private FieldUsage(Position pos, String sourceFileName, String referenceId, UsageKind kind) {
        super(pos, sourceFileName, true, null, UsageIdGen.getUsageId(sourceFileName, pos), referenceId, kind);
    }
}
