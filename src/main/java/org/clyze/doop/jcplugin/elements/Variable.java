package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.VarSymbol;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import javax.lang.model.element.ElementKind;

/**
 * Represents a local variable/parameter definition.
 */
public class Variable extends org.clyze.persistent.model.jvm.JvmVariable {

    public Variable(JavacFileInfo fInfo, long offset, VarSymbol sym, Method encMethod, boolean isInsideIIB) {
        super(fInfo.getPosition(offset, sym.getSimpleName().toString()),
                fInfo.getSourceFileName(),
                true,
                null,
                sym.getSimpleName().toString(),
                buildDoopId(sym, encMethod),
                sym.type.toString(),
                encMethod.getSymbolId(),
                sym.isLocal(),
                (sym.getKind() == ElementKind.PARAMETER),
                isInsideIIB);
    }

    /**
     * Generates a Doop id.
     *
     * @param sym           the javac symbol for the variable
     * @param encMethod     the jcplugin element for the declaring method
     * @return              the variable's Doop id
     */
    public static String buildDoopId(VarSymbol sym, Method encMethod) {
        String thisPrefix = sym.flatName().toString().equals("this") ? "@" : "";
        return encMethod.getSymbolId() + "/" + thisPrefix + sym.flatName().toString();
    }

}
