package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.ClassSymbol;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.persistent.model.Position;
import org.clyze.persistent.model.Symbol;

import static com.sun.tools.javac.code.Flags.ABSTRACT;
import static com.sun.tools.javac.code.Flags.FINAL;
import static com.sun.tools.javac.code.Flags.PUBLIC;
import static com.sun.tools.javac.code.Flags.PROTECTED;
import static com.sun.tools.javac.code.Flags.PRIVATE;

/**
 * Represents a class definition.
 */
public class Class extends org.clyze.persistent.model.jvm.JvmClass {

    public Class(JavacFileInfo fInfo, long offset, ClassSymbol sym, Symbol doopSymbol) {
        super(getPosition(fInfo, offset, sym),
                fInfo.getSourceFileName(),
                true,
                null,
                sym.getSimpleName().toString(),
                sym.packge().toString().replaceFirst("unnamed package", ""),
                buildDoopId(sym),
                sym.isInterface(),
                sym.isEnum(),
                sym.isStatic(),
                sym.isInner(),
                sym.isAnonymous(),
                ((sym.flags_field & ABSTRACT) != 0),
                ((sym.flags_field & FINAL) != 0),
                ((sym.flags_field & PUBLIC) != 0),
                ((sym.flags_field & PROTECTED) != 0),
                ((sym.flags_field & PRIVATE) != 0));

        if (doopSymbol instanceof Class)
            setDeclaringSymbolId(((org.clyze.persistent.model.jvm.JvmClass)doopSymbol).getSymbolId());
        else if(doopSymbol instanceof Method)
            setDeclaringSymbolId(((Method)doopSymbol).getSymbolId());
        else
            setDeclaringSymbolId("");
    }

    /**
     * Generates a Doop id.
     *
     * @param sym   the javac symbol for the class
     * @return      the class's Doop id
     */
    public static String buildDoopId(ClassSymbol sym) {
        return sym.flatName().toString();
    }

    private static Position getPosition(JavacFileInfo fInfo, long offset, ClassSymbol sym) {

        // The given position (i.e. offset from tree.pos) for a named type symbol in a type definition is always
        // at the beginning of the corresponding keyword, i.e. "class", "interface", "enum". For an abstract class
        // the given position is at the beginning of the "class" keyword instead of "abstract".

        if (sym.isAnonymous()) {
            // nop
        } else if(sym.isInterface())
            offset = fInfo.countWhitespaces(offset + "interface".length());
        else if(sym.isEnum())
            offset = fInfo.countWhitespaces(offset + "enum".length());
        else
            offset = fInfo.countWhitespaces(offset + "class".length());

        return fInfo.getPosition(offset, sym.getSimpleName().toString());
    }

}
