package org.clyze.doop.jcplugin.elements;

/**
 * This class represents a compiler-generated default constructor.
 */
public class DefaultConstructor extends CompilerGeneratedMethod {

    public DefaultConstructor(Class encClass) {
        super(encClass);
        setSymbolId(buildDoopId(encClass));
    }

    /**
     * Generates a Doop id for this constructor.
     *
     * @param encClass  the jcplugin element of the enclosing class
     * @return          the constructor's Doop id
     */
    public static String buildDoopId(Class encClass) {
        return "<" + encClass.getSymbolId() + ": void <init>()>";
    }

}
