package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.MethodSymbol;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.persistent.metadata.Printer;
import org.clyze.persistent.model.Position;
import org.clyze.persistent.model.Usage;
import org.clyze.persistent.model.jvm.JvmDynamicMethodInvocation;

public class MethodReference extends Usage {

    public MethodReference(JavacFileInfo fInfo, long offset, MethodSymbol sym, Printer p) {
        setPosition         (getPosition(fInfo, offset, sym));
        setSourceFileName   (fInfo.getSourceFileName());
        setSource           (true);
        setSymbolId         (Method.buildDoopId(p, sym));
        //setUsageKind        (UsageKind.METHOD_REFERENCE); // METHOD_REFERENCE is removed from UsageKind enum
    }

    public static Position getPosition(JavacFileInfo fInfo, long offset, MethodSymbol sym) {
        long startOffset = fInfo.countMethodRefPrefix(offset);
        long endOffset = startOffset + (sym.isConstructor() ? "new".length() : sym.getSimpleName().length());

        return new Position(
            fInfo.getLineNumber(startOffset),
            fInfo.getLineNumber(endOffset),
            fInfo.getColumnNumber(startOffset),
            fInfo.getColumnNumber(endOffset)
        );
    }

    /**
     * Creates a Doop id by generating the string of the invokedynamic
     * that corresponds to the method reference pointing to sym.
     *
     * @param sym         the javac symbol for the method
     * @param encMethod   the enclosing method
     * @return            the method's Doop id
     */
    public static String buildDoopId(MethodSymbol sym, Method encMethod) {
        String methodInMethodKey = encMethod.getSymbolId() + "/" +
            JvmDynamicMethodInvocation.genericId(sym.enclClass().flatName().toString(),
                                          sym.flatName().toString());
        return MethodInvocation.addUniqueSuffix(methodInMethodKey);
    }

}
