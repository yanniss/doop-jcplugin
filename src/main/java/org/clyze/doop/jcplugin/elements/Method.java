package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.MethodSymbol;
import com.sun.tools.javac.code.Symbol.VarSymbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.tree.JCTree.JCVariableDecl;
import com.sun.tools.javac.util.List;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.persistent.metadata.Printer;
import org.clyze.persistent.model.Position;

import static com.sun.tools.javac.code.Flags.ABSTRACT;
import static com.sun.tools.javac.code.Flags.NATIVE;
import static com.sun.tools.javac.code.Flags.SYNCHRONIZED;
import static com.sun.tools.javac.code.Flags.PUBLIC;
import static com.sun.tools.javac.code.Flags.PROTECTED;
import static com.sun.tools.javac.code.Flags.PRIVATE;
import static com.sun.tools.javac.code.Flags.SYNTHETIC;
import static com.sun.tools.javac.code.Flags.FINAL;
import static org.clyze.doop.jcplugin.util.TypeTransformer.eraseAndTranslateType;

/**
 * This class represents a method definition.
 */
public class Method extends org.clyze.persistent.model.jvm.JvmMethod {

    /**
     * This constructor is used for compiler-generated methods such as
     * default constructors, static initializers ("InitBlockStatic"),
     * and instance initalizers (such as field initializers, see
     * "InitBlockInstance").
     */
    Method() { }

    /**
     * Basic constructor.
     *
     * @param fInfo         file information
     * @param offsetStart   where the method definition begins (including any modifiers and annotations)
     * @param offsetEnd     where the method definition ends (i.e. right after the closing brace)
     * @param offsetName    where the method name appears in the method definition
     * @param sym           the method symbol
     * @param parameters    the parameters (variables) of the method
     * @param encClass      the enclosing class
     * @param p             the output printer to use for messages
     */
    public Method(JavacFileInfo fInfo, long offsetStart, long offsetEnd, long offsetName,
                  MethodSymbol sym, List<JCVariableDecl> parameters, Class encClass,
                  Printer p) {
        super(fInfo.getPosition(offsetName, getName(sym)),
                fInfo.getSourceFileName(),
                true,
                null,
                getName(sym),
                encClass.getSymbolId(),
                sym.getReturnType().toString(),
                buildDoopId(p, sym),
                getParamNames(parameters),
                getParamTypes(p, parameters),
                sym.isStatic(),
                encClass.isInterface(),
                ((sym.flags_field & ABSTRACT) != 0),
                ((sym.flags_field & NATIVE) != 0),
                ((sym.flags_field & SYNCHRONIZED) != 0),
                ((sym.flags_field & FINAL) != 0),
                ((sym.flags_field & SYNTHETIC) != 0),
                ((sym.flags_field & PUBLIC) != 0),
                ((sym.flags_field & PROTECTED) != 0),
                ((sym.flags_field & PRIVATE) != 0),
                getOuterPosition(fInfo, offsetStart, offsetEnd));
    }

    /**
     * Builds a Doop id for a method.
     *
     * @param p     the output printer to use for messages
     * @param sym   the javac symbol for the method
     * @return      the method's Doop id
     */
    public static String buildDoopId(Printer p, MethodSymbol sym) {

        StringBuilder doopId = new StringBuilder();
        List<VarSymbol> parameters = sym.getParameters();

        // Some special method names are quoted by Soot: to match
        // them, we have to apply the same logic.
        String methodName = sym.flatName().toString();
        if (methodName.equals("from") || methodName.equals("to") ||
            methodName.equals("with")) {
            methodName = "'" + methodName + "'";
        }

        doopId.append("<")
            .append(sym.enclClass().flatName().toString())
            .append(": ")
            .append(eraseAndTranslateType(p, sym.getReturnType()))
            .append(" ")
            .append(methodName)
            .append("(");

        // If method is constructor of an inner class, Doop id requires the parent class name as the
        // constructor's first parameter type.
        if(sym.isConstructor() && sym.enclClass().isInner()) {

            // Calling "encClass()" of the inner class symbol, does not return its parent class.
            // Therefore, we work with string manipulation: remove the last part from the inner
            // class' name, i.e. "$<inner-class-name>", and add it as first argument to the constructor.

            String encClassName = sym.enclClass().flatName().toString();

            doopId.append(encClassName, 0, encClassName.lastIndexOf("$"));
            if(parameters.size() > 0) {
                doopId.append(",");
            }
        }

        // Add the fully qualified types of method parameters
        for(int i = 0; i < parameters.size(); i++) {
            Type paramType = parameters.get(i).type;
            String doopType = eraseAndTranslateType(p, paramType);
            doopId.append(doopType).append(i < parameters.size() - 1 ? "," : "");
        }
        doopId.append(")>");

        return doopId.toString();
    }

    /**
     * In case of constructor method, give it its class' name instead of "<init>".
     *
     * @param sym   the symbol of the method
     * @return      the name of the method
     */
    private static String getName(MethodSymbol sym) {
        return sym.isConstructor() ? sym.enclClass().getSimpleName().toString() : sym.getSimpleName().toString();
    }

    /**
     * Returns the names of the parameters of a method (given as a list).
     *
     * @param parameters   the list of parameters
     * @return             an array of parameter names
     */
    private static String[] getParamNames(List<JCVariableDecl> parameters) {

        String[] paramNames = new String[parameters.length()];
        int counter = 0;

        for(JCVariableDecl param : parameters) {
            paramNames[counter] = param.name.toString();
            counter++;
        }

        return paramNames;
    }

    private static String[] getParamTypes(Printer p, List<JCVariableDecl> parameters) {

        String[] paramTypes = new String[parameters.length()];
        int counter = 0;

        for (JCVariableDecl param : parameters) {
//            paramTypes[counter] = eraseAndTranslateType(p, param.vartype.type);
            paramTypes[counter] = param.vartype.type.toString();
            counter++;
        }

        return paramTypes;
    }

    /**
     * Returns the position/range where the method definition begins
     * and ends. If the starting or ending lines contain nothing else
     * except the method definition and whitespaces, it sets the
     * corresponding column equal to zero (note that column numbers
     * start counting from "1").
     *
     * @param fInfo        file information
     * @param offsetStart  starting offset
     * @param offsetEnd    end offset
     * @return             the position
     */
    private static Position getOuterPosition(JavacFileInfo fInfo, long offsetStart, long offsetEnd) {

        long startPosLine   = fInfo.getLineNumber(offsetStart);
        long startPosColumn = fInfo.getColumnNumber(offsetStart);
        long endPosLine     = fInfo.getLineNumber(offsetEnd);
        long endPosColumn   = fInfo.getColumnNumber(offsetEnd);
        long wsc; // WhiteSpaces Counted

        /*
         * Check if there is anything else, except the method definition, in the line where the method definition begins
         * Note that "startPosColumn" starts at "1", thus the "(startPosColumn - 1)" below
         */

        // Find the offset at the start of the line where the method definition begins
        long offsetLineStart = offsetStart - (startPosColumn - 1);

        // Count the whitespaces from the start of the line till the column where the method definition begins
        wsc = fInfo.countWhitespaces(offsetLineStart) - offsetLineStart;

        // If there is nothing else (i.e. only whitespaces) in the line before the column where the method definition
        // begins, then set "start position column" equal to zero
        if(wsc == startPosColumn - 1) {
            startPosColumn = 0;
        }

        /*
         * Check if there is anything else, except the method definition, in the line where the method definition ends
         */

        // Count the whitespaces from the column where the method definition ends and onwards
        wsc = fInfo.countWhitespaces(offsetEnd) - offsetEnd;

        // If the method definition ends on a different line than the first non-whitespace character, then it means that
        // there is nothing else in the ending line (i.e. only whitespaces); therefore set "end position column" equal
        // to a huge value (simulating infinity)
        if(fInfo.getLineNumber(offsetEnd + wsc) != endPosLine) {
            endPosColumn = Integer.MAX_VALUE;
        }

        return new Position(startPosLine, endPosLine, startPosColumn, endPosColumn);
    }

}
