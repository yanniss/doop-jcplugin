package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.VarSymbol;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.persistent.metadata.Printer;

public class StringConstant extends org.clyze.persistent.model.jvm.JvmStringConstant {

    public StringConstant(JavacFileInfo fInfo, VarSymbol fieldSymbol, long beginPosition,
                          long endPosition, String value, Printer p) {
        super(fInfo.getPosition(beginPosition, endPosition),
            fInfo.getSourceFileName(),
            true,
            Field.buildDoopId(p, fieldSymbol),
            value);
    }

}
