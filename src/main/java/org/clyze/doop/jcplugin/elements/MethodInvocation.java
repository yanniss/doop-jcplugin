package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.code.Symbol.MethodSymbol;
import java.util.HashMap;
import javax.lang.model.element.ElementKind;

import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.persistent.model.Position;

/**
 * Represents a method invocation (including constructor invocations in object instantiations).
 */
public class MethodInvocation extends org.clyze.persistent.model.jvm.JvmMethodInvocation {

    private static final HashMap<String, Integer> methodInvocationCounter = new HashMap<>();

    /**
     * Basic constructor for method invocations/
     *
     * @param fInfo         file information
     * @param offset        the offset of the invocation
     * @param sym           the symbol of the invocation
     * @param encMethod     the enclosing method
     * @param isSuper       'true' if it an invocation to the supertype
     * @param isThis        'true' if it is an invocation to 'this'
     * @param isInsideIIB   is inside instance initializer block
     */
    public MethodInvocation(JavacFileInfo fInfo, long offset, Symbol sym, Method encMethod, boolean isSuper, boolean isThis, boolean isInsideIIB) {
        setPosition         (getPosition(fInfo, offset, sym, isSuper, isThis));
        setSourceFileName   (fInfo.getSourceFileName());
        setSource           (true);
        setName             (isSuper ? "super" : sym.getSimpleName().toString());
        setSymbolId         (buildDoopId(sym, encMethod));
        setInvokingMethodId (encMethod.getSymbolId());
        setInIIB            (isInsideIIB);
    }

    /**
     * Builds a method invocation that corresponds to a method
     * reference. Used to map the creation of invokedynamic
     * instructions where method references appear in the source.
     *
     * @param fInfo         file information
     * @param offset        the offset of the invocation
     * @param sym           the method pointed to by the reference
     * @param encMethod     the enclosing method
     * @param isInsideIIB   is inside instance initializer block
     */
    public MethodInvocation(JavacFileInfo fInfo, long offset, MethodSymbol sym, Method encMethod, boolean isInsideIIB) {
        setPosition         (MethodReference.getPosition(fInfo, offset, sym));
        setSourceFileName   (fInfo.getSourceFileName());
        setName             ("<method reference>");
        setSymbolId         (MethodReference.buildDoopId(sym, encMethod));
        setInvokingMethodId (encMethod.getSymbolId());
        setInIIB            (isInsideIIB);
    }

    /**
     * Generates a Doop id for a method invocation.
     *
     * @param sym           the javac symbol for the invoked method, or class in case of object instantiation
     * @param encMethod     the jcplugin element for the caller/enclosing method
     * @return              the method invocation's Doop id
     */
    public static String buildDoopId(Symbol sym, Method encMethod) {
        String methodName = (sym.getKind() == ElementKind.CLASS ? "<init>" : sym.getSimpleName().toString());
        String encClassName = (sym.getKind() == ElementKind.CLASS ? sym.flatName() : sym.enclClass().flatName()).toString();
        String methodInMethodKey = encMethod.getSymbolId() + "/" + encClassName + "." + methodName;
        return addUniqueSuffix(methodInMethodKey);
    }

    /**
     * Helper method that also gets called by method references.
     *
     * @param methodInMethodKey The prefix of an instruction id
     * @return                  methodInMethodKey + unique suffix
     */
    public static String addUniqueSuffix(String methodInMethodKey) {
        Integer counter = methodInvocationCounter.get(methodInMethodKey);

        counter = (counter == null ? 0 : counter + 1);
        methodInvocationCounter.put(methodInMethodKey, counter);

        return methodInMethodKey + "/" + counter;
    }

    private Position getPosition(JavacFileInfo fInfo, long offset, Symbol sym, boolean isSuper, boolean isThis) {
        long length;

        if (isSuper) // if specific call to a constructor through "super" keyword
            length = "super".length();
        else if (isThis) // if specific call to a constructor through "this" keyword
            length = "this".length();
        else if(sym.isConstructor())
            length = sym.enclClass().getSimpleName().length();
        else
            length = sym.getSimpleName().length();

        return new Position(
            fInfo.getLineNumber(offset),
            fInfo.getColumnNumber(offset),
            fInfo.getColumnNumber(offset) + length
        );
    }

}
