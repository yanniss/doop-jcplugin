package org.clyze.doop.jcplugin.elements;

/**
 * Represents a static initializer block.
 *
 * An "InitBlockStatic" instance is not to be reported; it simply
 * helps jcplugin to report other elements by disguising itself as
 * "Method" and thus removing the need for case control in the
 * visitor(s) for executing the same action.
 */
public class InitBlockStatic extends CompilerGeneratedMethod {

    public InitBlockStatic(Class encClass) {
        super(encClass);
        setSymbolId(buildDoopId(encClass));
        setStatic(true);
        setName("<clinit>");
        setReturnType("void");
        setParams(new String[0]);
        setParamTypes(new String[0]);
    }

    /**
     * Generates a Doop id for this static initializer.
     *
     * @param encClass  the jcplugin element of the enclosing class
     * @return          the initializer block's Doop id
     */
    public static String buildDoopId(Class encClass) {
        return "<" + encClass.getSymbolId() + ": void <clinit>()>";
    }

}
