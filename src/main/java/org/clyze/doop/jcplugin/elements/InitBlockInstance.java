package org.clyze.doop.jcplugin.elements;

/**
 * Represents an instance initializer block.
 *
 * An "InitBlockStatic" instance is not to be reported; it simply
 * helps jcplugin to report other elements by disguising itself as
 * "Method" and thus removing the need for case control in the
 * visitor(s) for executing the same action.
 */
public class InitBlockInstance extends CompilerGeneratedMethod {

    public InitBlockInstance(Class encClass) {
        super(encClass);
    }

}
