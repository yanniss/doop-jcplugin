package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.VarSymbol;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.persistent.metadata.Printer;

import static org.clyze.doop.jcplugin.util.TypeTransformer.eraseAndTranslateType;

/**
 * Represents a field definition.
 */
public class Field extends org.clyze.persistent.model.jvm.JvmField {

    public Field(JavacFileInfo fInfo, long offset, VarSymbol sym, Printer p) {
        super(fInfo.getPosition(offset, sym.getSimpleName().toString()),
                fInfo.getSourceFileName(),
                true,
                null,
                sym.getSimpleName().toString(),
                buildDoopId(p, sym),
                eraseAndTranslateType(p, sym.type),
                Class.buildDoopId(sym.enclClass()),
                sym.isStatic());
    }

    /**
     * Generates a Doop id.
     *
     * @param p     the output printer to use for messages
     * @param sym   the javac symbol for the field
     * @return      the field's Doop id
     */
    public static String buildDoopId(Printer p, VarSymbol sym) {
        return "<" +
            sym.enclClass().flatName() + ": " +
            eraseAndTranslateType(p, sym.type) + " " +
            sym.getSimpleName() +
            ">";
    }

}
