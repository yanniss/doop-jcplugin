package org.clyze.doop.jcplugin.elements;

public abstract class CompilerGeneratedMethod extends Method {
    public CompilerGeneratedMethod(Class encClass) {
        setDeclaringClassId(encClass.getSymbolId());
        setSource(true);
    }
}
