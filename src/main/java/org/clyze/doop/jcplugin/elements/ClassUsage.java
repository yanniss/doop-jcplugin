package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.ClassSymbol;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.doop.jcplugin.util.UsageIdGen;
import org.clyze.persistent.model.Position;
import org.clyze.persistent.model.Usage;
import org.clyze.persistent.model.UsageKind;

/**
 * Represents a class usage, i.e. appropriate for all class occurrences except in definitions.
 */
public class ClassUsage extends Usage {
    public ClassUsage(JavacFileInfo fInfo, long offset, ClassSymbol sym) {
        this(fInfo.getPosition(offset, sym.getSimpleName().toString()),
                fInfo.getSourceFileName(),
                Class.buildDoopId(sym));
    }

    private ClassUsage(Position pos, String sourceFileName, String referenceId) {
        super(pos, sourceFileName, true, null, UsageIdGen.getUsageId(sourceFileName, pos), referenceId, UsageKind.TYPE);
    }
}
