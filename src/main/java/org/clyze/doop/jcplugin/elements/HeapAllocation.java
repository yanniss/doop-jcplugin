package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.TypeSymbol;
import java.util.HashMap;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.persistent.metadata.Printer;
import org.clyze.doop.jcplugin.util.TypeTransformer;
import org.clyze.persistent.model.Position;

/**
 * Represents a heap allocation site.
 */
public class HeapAllocation extends org.clyze.persistent.model.jvm.JvmHeapAllocation {

    private static final HashMap<String, Integer> heapAllocationCounter = new HashMap<>();

    public HeapAllocation(JavacFileInfo fInfo, long beginPosition, long endPosition,
                          TypeSymbol tsym, boolean isArray,
                          Method encMethod, boolean isInsideIIB, Printer p) {
        setPosition             (getPosition(fInfo, beginPosition, endPosition));
        setSourceFileName       (fInfo.getSourceFileName());
        setSource               (true);
        setSymbolId             (buildDoopId(p, tsym, isArray, encMethod));
        setAllocatedTypeId      (isArray ? "java.lang.Object" : TypeTransformer.eraseGenericType(p, tsym.type));
        setAllocatingMethodId   (encMethod.getSymbolId());
        setInIIB                (isInsideIIB);
        setArray                (isArray);
    }

    public static String buildDoopId(Printer p, TypeSymbol tsym,
                                     boolean isArray, Method encMethod) {
        String type             = TypeTransformer.eraseGenericType(p, tsym.type);
        String allocInMethodKey = encMethod.getSymbolId() + " " + type + (isArray ? "[]" : "");
        Integer counter         = heapAllocationCounter.get(allocInMethodKey);

        counter = (counter == null ? 0 : counter + 1);
        heapAllocationCounter.put(allocInMethodKey, counter);

        return encMethod.getSymbolId() + "/new " + type + (isArray ? "[]" : "") + "/" + counter;
    }

    private Position getPosition(JavacFileInfo fInfo, long beginOffset, long endOffset) {
        return new Position(
            fInfo.getLineNumber(beginOffset),
            fInfo.getLineNumber(endOffset),
            fInfo.getColumnNumber(beginOffset),
            fInfo.getColumnNumber(endOffset)
        );
    }

}
