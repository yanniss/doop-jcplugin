package org.clyze.doop.jcplugin.elements;

import com.sun.tools.javac.code.Symbol.VarSymbol;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.doop.jcplugin.util.UsageIdGen;
import org.clyze.persistent.model.Position;
import org.clyze.persistent.model.Usage;
import org.clyze.persistent.model.UsageKind;

/**
 * Represents a local variable/parameter usage, i.e. appropriate for
 * all variable occurrences except in definitions.
 */
public class VariableUsage extends Usage {
    public VariableUsage(JavacFileInfo fInfo, long offset, VarSymbol sym, UsageKind kind, Method encMethod) {
        this(fInfo.getPosition(offset, sym.getSimpleName().toString()),
                fInfo.getSourceFileName(),
                Variable.buildDoopId(sym, encMethod),
                kind);
    }

    private VariableUsage(Position pos, String sourceFileName, String referenceId, UsageKind kind) {
        super(pos, sourceFileName, true, null, UsageIdGen.getUsageId(sourceFileName, pos), referenceId, kind);
    }
}
