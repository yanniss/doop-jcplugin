package org.clyze.doop.jcplugin.visitors;

import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.tree.TreeScanner;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.JCTree.*;
import java.util.Stack;
import javax.lang.model.element.ElementKind;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.persistent.metadata.Configuration;

import static com.sun.tools.javac.code.Flags.*;

/**
 * Simply traverses and prints the AST.
 *
 * Note: This class is intended to help the developer understand the structure of the javac AST.
 *       There is no need (at least for now ^^) to be used in production.
 */
public class PrintScanner extends TreeScanner {

    static class ASTNodeData {
        final Symbol sym;
        final long position;

        ASTNodeData(Symbol sym, long position) {
            this.sym = sym;
            this.position = position;
        }
    }


    private final JavacFileInfo fInfo;
    private final Configuration configuration;
    final Stack<ASTNodeData> nodesStack = new Stack<>();
    private int isAutoGen = 0;

    public PrintScanner(JavacFileInfo fInfo, Configuration configuration) {
        this.fInfo = fInfo;
        this.configuration = configuration;
    }

    /**
     * Used to print the AST together with various node information.
     *
     * @param visit    node information (may include control codes such
     *                 as color escape sequences)
     * @param args     the arguments to print
     */
    private void print(String visit, String... args) {

        StringBuilder spaces = new StringBuilder();
        for(int i = nodesStack.size(); i > 0; i--)
            spaces.append("   ");
        configuration.printer.print(spaces.toString());

        configuration.printer.print(visit);
        for (String arg : args)
            configuration.printer.print(" " + arg);
        configuration.printer.println("");
    }

    /**
     * A "visit*" method should call "open" before visiting any children AST nodes.
     *
     * @param visit     A string describing a specific called visit method
     * @param args      A set of strings to print
     */
    private void open(String visit, String... args) {

        print((isAutoGen > 0 ? "\033[36m" : "\033[30m") + visit + "\033[0m", args);
        nodesStack.push(null);
    }

    /**
     * A "visit*" method should call "open" before visiting any children AST nodes.
     *
     * @param visit     A string describing a specific called visit method
     * @param node      A javac AST node
     * @param sym       A javac symbol
     */
    private void open(String visit, JCTree node, Symbol sym) {

        if(sym != null) {
            long startLine = fInfo.getLineNumber(node.pos);
            long startColumn = fInfo.getColumnNumber(node.pos);
            long length = sym.getSimpleName().toString().length();

            print(
                (isAutoGen > 0 ? "\033[36m" : "\033[30m") + visit + "\033[0m"
            ,   "\033[0m\"\033[0m" + sym.getSimpleName() + "\033[0m\""
            ,   "\033[0m\"\033[33m" + sym.flatName() + "\033[0m\""
            ,   "\033[0mpos:\033[34m[" + startLine + "," + startColumn + "-" + (startColumn + length) + "]\033[0m"
            ,   "\033[0mkind:\033[35m" + sym.getKind() + "\033[0m"
            ,   "\033[0msid:<\033[32m" + sym.hashCode() + "\033[0m>"
            ,   "\033[0mF:<\033[32m" + (sym.flags()) + "\033[0m>"
            );

            nodesStack.push(new ASTNodeData(sym, node.pos));
        }
        else {
            print((isAutoGen > 0 ? "\033[36m" : "\033[31m") + visit + "\033[0m", node.toString());
            nodesStack.push(null);
        }
    }

    /**
     * A "visit*" method should call "close" after visiting any children AST nodes.
     *
     * @param visit     A string describing a specific called visit method
     * @param args      A set of strings to print
     */
    private void close(String visit, String... args) {
        nodesStack.pop();
        //print(visit, args);
    }

    /**
     * Find if a method is compiler-generated based on its symbol's flags.
     * As an alternative implementation, we could use "existsInFile".
     *
     * @param sym   the symbol to check
     * @return      true if compiler-generated
     */
    private boolean isAutoGenConstructorDef(Symbol.MethodSymbol sym) {
        return (sym.flags_field & (SYNTHETIC | GENERATEDCONSTR | ANONCONSTR)) != 0;
    }

    /**
     * Find if a statement is a compiler-generated call to "super" method.
     *
     * @param node   the invocation node
     * @return       true if compiler-generated
     */
    private boolean isAutoGenCallSuper(JCExpressionStatement node) {
        return node.toString().equals("super();") && !fInfo.existsInSource("super();", node.pos);
    }

    private String getInitBlockDescription(JCBlock node) {

        return nodesStack.peek() != null && nodesStack.peek().sym.getKind() == ElementKind.CLASS
            ? ("kind:\033[35m" + (node.isStatic() ? "STATIC" : "INSTANCE") + "_INITIALIZER_BLOCK\033[0m")
            : "";
    }

    /***************************************************************************
     * Visitor methods
     **************************************************************************/

    @Override
    public void visitAnnotatedType(JCAnnotatedType tree) {

        open("AnnotatedType", tree.toString());

        super.visitAnnotatedType(tree);

        close("AnnotatedType");
    }

    @Override
    public void visitAnnotation(JCAnnotation tree) {

        open("Annotation", tree.toString());

        super.visitAnnotation(tree);

        close("Annotation");
    }

    @Override
    public void visitApply(JCMethodInvocation tree) {

        open("Apply", tree.meth.toString(), tree.args.toString(), tree.typeargs.toString());

        super.visitApply(tree);

        close("Apply");
    }

    @Override
    public void visitAssert(JCAssert tree) {

        open("Assert");

        super.visitAssert(tree);

        close("Assert");
    }

    @Override
    public void visitAssign(JCAssign tree) {

        open("Assign", tree.toString());

        super.visitAssign(tree);

        close("Assign");
    }

    @Override
    public void visitAssignop(JCAssignOp tree) {

        open("Assignop");

        super.visitAssignop(tree);

        close("Assignop");
    }

    @Override
    public void visitBinary(JCBinary tree) {

        open("Binary", tree.toString());

        super.visitBinary(tree);

        close("Binary");
    }

    @Override
    public void visitBlock(JCBlock tree) {

        open("Block", getInitBlockDescription(tree));

        super.visitBlock(tree);

        close("Block");
    }

    @Override
    public void visitBreak(JCBreak tree) {

        open("Break");

        super.visitBreak(tree);

        close("Break");
    }

    @Override
    public void visitCase(JCCase tree) {

        open("Case");

        super.visitCase(tree);

        close("Case");
    }

    @Override
    public void visitCatch(JCCatch tree) {

        open("Catch");

        super.visitCatch(tree);

        close("Catch");
    }

    @Override
    public void visitClassDef(JCClassDecl tree) {

        open("ClassDef", tree, tree.sym);

        super.visitClassDef(tree);

        close("ClassDef");
    }

    @Override
    public void visitConditional(JCConditional tree) {

        open("Conditional");

        super.visitConditional(tree);

        close("Conditional");
    }

    @Override
    public void visitContinue(JCContinue tree) {

        open("Continue");

        super.visitContinue(tree);

        close("Continue");
    }

    @Override
    public void visitDoLoop(JCDoWhileLoop tree) {

        open("DoLoop");

        super.visitDoLoop(tree);

        close("DoLoop");
    }

    @Override
    public void visitErroneous(JCErroneous tree) {

        open("Erroneous");

        super.visitErroneous(tree);

        close("Erroneous");
    }

    @Override
    public void visitExec(JCExpressionStatement tree) {

        boolean isAutoGen = isAutoGenCallSuper(tree);
        if(isAutoGen) { this.isAutoGen++; }

        open("Exec", tree.toString());

        super.visitExec(tree);

        close("Exec");

        if(isAutoGen) { this.isAutoGen--; }
    }

    @Override
    public void visitForLoop(JCForLoop tree) {

        open("ForLoop", "(" + tree.init + "; " + tree.cond + "; " + tree.step + ")");

        super.visitForLoop(tree);

        close("ForLoop");
    }

    @Override
    public void visitForeachLoop(JCEnhancedForLoop tree) {

        open("ForeachLoop");

        super.visitForeachLoop(tree);

        close("ForeachLoop");
    }

    @Override
    public void visitIdent(JCIdent tree) {

        open("Ident", tree, tree.sym);

        super.visitIdent(tree);

        close("Ident");
    }

    @Override
    public void visitIf(JCIf tree) {

        open("If", tree.getCondition().toString());

        super.visitIf(tree);

        close("If");
    }

    @Override
    public void visitImport(JCImport tree) {

        open("Import");

        super.visitImport(tree);

        close("Import");
    }

    @Override
    public void visitIndexed(JCArrayAccess tree) {

        open("Indexed");

        super.visitIndexed(tree);

        close("Indexed");
    }

    @Override
    public void visitLabelled(JCLabeledStatement tree) {

        open("Labelled");

        super.visitLabelled(tree);

        close("Labelled");
    }

    @Override
    public void visitLambda(JCLambda tree) {

        open("Lambda", tree, null);

        super.visitLambda(tree);

        close("Lambda");
    }

    @Override
    public void visitLetExpr(LetExpr tree) {

        open("LetExpr");

        super.visitLetExpr(tree);

        close("LetExpr");
    }

    @Override
    public void visitLiteral(JCLiteral tree) {

        open("Literal", tree.toString());

        super.visitLiteral(tree);

        close("Literal");
    }

    @Override
    public void visitMethodDef(JCMethodDecl tree) {

        boolean isAutoGen = tree.sym != null && isAutoGenConstructorDef(tree.sym);
        if(isAutoGen) { this.isAutoGen++; }

        open("MethodDef", tree, tree.sym);

        super.visitMethodDef(tree);

        close("MethodDef");

        if(isAutoGen) { this.isAutoGen--; }
    }

    @Override
    public void visitModifiers(JCModifiers tree) {

        open("Modifiers", tree.getFlags().toString());

        super.visitModifiers(tree);

        close("Modifiers");
    }

    @Override
    public void visitNewArray(JCNewArray tree) {

        open("NewArray");

        super.visitNewArray(tree);

        close("NewArray");
    }

    @Override
    public void visitNewClass(JCNewClass tree) {

        open("NewClass");

        super.visitNewClass(tree);

        close("NewClass");
    }

    @Override
    public void visitParens(JCParens tree) {

        open("Parens", tree.toString());

        super.visitParens(tree);

        close("Parens");
    }

    @Override
    public void visitReference(JCMemberReference tree) {

        open("Reference", tree, tree.sym);

        super.visitReference(tree);

        close("Reference");
    }

    @Override
    public void visitReturn(JCReturn tree) {

        open("Return");

        super.visitReturn(tree);

        close("Return");
    }

    @Override
    public void visitSelect(JCFieldAccess tree) {

        open("Select", tree, tree.sym);

        super.visitSelect(tree);

        close("Select");
    }

    @Override
    public void visitSkip(JCSkip tree) {

        open("Skip");

        super.visitSkip(tree);

        close("Skip");
    }

    @Override
    public void visitSwitch(JCSwitch tree) {

        open("Switch");

        super.visitSwitch(tree);

        close("Switch");
    }

    @Override
    public void visitSynchronized(JCSynchronized tree) {

        open("Synchronized");

        super.visitSynchronized(tree);

        close("Synchronized");
    }

    @Override
    public void visitThrow(JCThrow tree) {

        open("Throw");

        super.visitThrow(tree);

        close("Throw");
    }

    @Override
    public void visitTopLevel(JCCompilationUnit tree) {

        open("TopLevel");

        super.visitTopLevel(tree);

        close("TopLevel");
    }

    @Override
    public void visitTree(JCTree tree) {

        open("Tree");

        super.visitTree(tree);

        close("Tree");
    }

    @Override
    public void visitTry(JCTry tree) {

        open("Try");

        super.visitTry(tree);

        close("Try");
    }

    @Override
    public void visitTypeApply(JCTypeApply tree) {

        open("TypeApply");

        super.visitTypeApply(tree);

        close("TypeApply");
    }

    @Override
    public void visitTypeArray(JCArrayTypeTree tree) {

        open("TypeArray", tree.toString());

        super.visitTypeArray(tree);

        close("TypeArray");
    }

    @Override
    public void visitTypeBoundKind(TypeBoundKind tree) {

        open("TypeBoundKind");

        super.visitTypeBoundKind(tree);

        close("TypeBoundKind");
    }

    @Override
    public void visitTypeCast(JCTypeCast tree) {

        open("TypeCast");

        super.visitTypeCast(tree);

        close("TypeCast");
    }

    @Override
    public void visitTypeIdent(JCPrimitiveTypeTree tree) {

        open("TypeIdent", tree.toString());

        super.visitTypeIdent(tree);

        close("TypeIdent");
    }

    @Override
    public void visitTypeIntersection(JCTypeIntersection tree) {

        open("TypeIntersection", tree, null);

        super.visitTypeIntersection(tree);

        close("TypeIntersection");
    }

    @Override
    public void visitTypeParameter(JCTypeParameter tree) {

        open("TypeParameter");

        super.visitTypeParameter(tree);

        close("TypeParameter");
    }

    @Override
    public void visitTypeTest(JCInstanceOf tree) {

        open("TypeTest");

        super.visitTypeTest(tree);

        close("TypeTest");
    }

    @Override
    public void visitTypeUnion(JCTypeUnion tree) {

        open("TypeUnion");

        super.visitTypeUnion(tree);

        close("TypeUnion");
    }

    @Override
    public void visitUnary(JCUnary tree) {

        open("Unary");

        super.visitUnary(tree);

        close("Unary");
    }

    @Override
    public void visitVarDef(JCVariableDecl tree) {

        open("VarDef", tree, tree.sym);

        super.visitVarDef(tree);

        close("VarDef");
    }

    @Override
    public void visitWhileLoop(JCWhileLoop tree) {

        open("WhileLoop", tree.getCondition().toString());

        super.visitWhileLoop(tree);

        close("WhileLoop");
    }

    @Override
    public void visitWildcard(JCWildcard tree) {

        open("WildCard");

        super.visitWildcard(tree);

        close("WildCard");
    }

}
