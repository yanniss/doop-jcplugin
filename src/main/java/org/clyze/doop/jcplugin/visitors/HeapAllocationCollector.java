package org.clyze.doop.jcplugin.visitors;

import com.sun.tools.javac.code.Symbol.TypeSymbol;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.tree.TreeScanner;
import javax.lang.model.element.ElementKind;
import java.util.List;
import java.util.Set;
import org.clyze.doop.jcplugin.elements.HeapAllocation;
import org.clyze.doop.jcplugin.elements.InitBlockInstance;
import org.clyze.doop.jcplugin.elements.Method;

/**
 *
 */
public class HeapAllocationCollector extends TreeScanner {

    final ElementScanner scanner;

    HeapAllocationCollector(ElementScanner scanner) {
        this.scanner = scanner;
    }

    public void visitNewClass(JCNewClass tree) {
        if (tree.type == null)
            return;

        TypeSymbol tsym;
        long beginPosition, endPosition;

        if(tree.clazz instanceof JCIdent && ((JCIdent)tree.clazz).sym.getKind() == ElementKind.ENUM_CONSTANT) {
            // Enumeration fields.

            tsym          = ((JCIdent)tree.clazz).sym.type.tsym;
            beginPosition = tree.pos;
            endPosition   = scanner.fInfo.countRegexLength(beginPosition, "\\A(\\([\\w\\.]+\\))*");
        } else {
            // Allocations using the "new" keyword:
            //  1) T item = new T();
            //  2) T item = new T() {}; // instantiation of anonymous class

            tsym          = tree.type.tsym;
            beginPosition = tree.pos;
            endPosition   = tree.clazz.getEndPosition(scanner.endPositions);
        }

        reportHeapAllocation(beginPosition, endPosition, tsym, false);
    }

    @Override
    public void visitNewArray(JCNewArray tree) {
        if (tree.type == null)
            return;

        TypeSymbol tsym = ((Type.ArrayType)tree.type).getComponentType().tsym;
        long beginPosition, endPosition;

        if (tree.elemtype == null) {
            // Array allocations without using the "new" keyword; i.e. only through array literal:
            //  1) T[] items = { t1, t2, t3 };

            beginPosition = tree.pos;
            endPosition   = beginPosition + 1;
        } else {
            // Array allocations using the "new" keyword:
            //  1) T[] items = new T[3];
            //  2) T[] items = new T[] { t1, t2, t3};

            beginPosition = tree.pos;
            endPosition   = tree.elemtype.getEndPosition(scanner.endPositions);
        }

        reportHeapAllocation(beginPosition, endPosition, tsym, true);
    }

    private void reportHeapAllocation(long beginPosition, long endPosition, TypeSymbol tsym, boolean isArray) {
        Set<org.clyze.persistent.model.jvm.JvmHeapAllocation> heapAllocations = scanner.fInfo.getElements().jvmHeapAllocations;
        Method method = scanner.methods.peek();
        List<String> constructorDoopIds = scanner.classesConstructors.peek();

        if(method instanceof InitBlockInstance) {
            for(String constructorDoopId : constructorDoopIds) {
                method.setSymbolId(constructorDoopId);
                heapAllocations.add(new HeapAllocation(
                    scanner.fInfo,
                    beginPosition,
                    endPosition,
                    tsym,
                    isArray,
                    scanner.methods.peek(),
                    scanner.methods.peek() instanceof InitBlockInstance,
                    scanner.configuration.printer
                ));
            }
        } else {
            heapAllocations.add(new HeapAllocation(
                scanner.fInfo,
                beginPosition,
                endPosition,
                tsym,
                isArray,
                scanner.methods.peek(),
                scanner.methods.peek() instanceof InitBlockInstance,
                scanner.configuration.printer
            ));
        }
    }

}
