package org.clyze.doop.jcplugin.visitors;

import com.sun.tools.javac.code.Symbol.VarSymbol;
import com.sun.tools.javac.tree.JCTree;
import com.sun.tools.javac.tree.TreeScanner;
import javax.lang.model.element.ElementKind;
import org.clyze.doop.jcplugin.elements.StringConstant;

import static com.sun.tools.javac.code.Flags.FINAL;
import static com.sun.tools.javac.code.Flags.STATIC;

/**
 * This scanner collects the string literals that:
 * - initialize a static final field in its declaration,
 * - and this field gets inlined by javac wherever it is used.
 */
public class StringConstantCollector extends TreeScanner {

    private final ElementScanner scanner;

    StringConstantCollector(ElementScanner scanner) {

        this.scanner = scanner;
    }

    @Override
    public void visitVarDef(JCTree.JCVariableDecl tree) {

        if(tree.sym == null) {
            return;
        }

        VarSymbol sym         = tree.sym;
        Object constValue     = sym.getConstantValue();
        boolean isField       = sym.getKind() == ElementKind.FIELD;
        boolean isStaticFinal = (tree.mods.flags & (STATIC|FINAL)) == (STATIC|FINAL);
        boolean isString      = tree.vartype.type.tsym.flatName().toString().equals("java.lang.String");

        if(isField && isStaticFinal && isString && constValue != null) {
            scanner.fInfo.getElements().jvmStringConstants.add(new StringConstant(
                scanner.fInfo,
                sym,
                tree.init.getStartPosition(),
                tree.init.getEndPosition(scanner.endPositions),
                constValue.toString(),
                scanner.configuration.printer
            ));
        }
    }

}
