package org.clyze.doop.jcplugin;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import javax.tools.ToolProvider;
import org.clyze.persistent.metadata.Configuration;

/**
 * Invokes the system's Java compiler, enabling the scavenge plugin automagically.
 * Use -sd path/to/json/output to indicate the scavenge output dir.
 */
public class Main {

    /** The command option to declare directory for JSON output files. */
    public static final String FLAG_JSON_OUTPUT = "-sd";
    public static final String FLAG_ENCODING = "-encoding";

    public static void main(String[] args) {

        String[] compilerArgs = args;

        if (args.length > 0) {
            try {
                URLClassLoader cl = (URLClassLoader)ClassLoader.getSystemClassLoader();
                URL[] urls = cl.getURLs();
                String[] ourClasspath = new String[urls.length];

                for (int i = 0; i < urls.length; i++)
                    ourClasspath[i] = urls[i].getFile();

                // We favor readability over efficiency
                String outDir = null;
                List<String> filteredCompilerArgs = new ArrayList<>();

                for (int i = 0; i < args.length; i++) {
                    if (args[i].equals(FLAG_JSON_OUTPUT)) {
                        if (++i == args.length)
                            throw new Exception("Flag " + FLAG_JSON_OUTPUT + " requires an argument");
                        outDir = args[i];
                    } else if(args[i].equals(FLAG_ENCODING)) {
                        if (++i == args.length)
                            throw new Exception("Flag " + FLAG_ENCODING + " requires an argument");
                        Configuration.encoding = args[i];
                        filteredCompilerArgs.add(FLAG_ENCODING);
                        filteredCompilerArgs.add(Configuration.encoding);
                    } else
                        filteredCompilerArgs.add(args[i]);
                }

                List<String> submittedCompilerArgs = new ArrayList<>();

                submittedCompilerArgs.add("-processorpath");
                submittedCompilerArgs.add(String.join(File.pathSeparator, ourClasspath));
                submittedCompilerArgs.add("-Xplugin:TypeInfoPlugin" + (outDir == null ? "" : " " + createDir(outDir)));
                submittedCompilerArgs.addAll(filteredCompilerArgs);

                compilerArgs = submittedCompilerArgs.toArray(new String[0]);
            }
            catch(Exception ex) {
                ex.printStackTrace();
                exit(-1);
            }
        }

        int returnCode = ToolProvider.getSystemJavaCompiler().run(System.in, System.out, System.err, compilerArgs);

        exit(returnCode);
    }

    private static String createDir(String outDir) {
        File dir = new File(outDir);
        if (!dir.mkdirs() && !dir.exists())
            System.err.println("Could not create directory " + outDir);
        return dir.toString();
    }

    /**
     * Alternative to calling System.exit() for smooth integration
     * with the compiler (for example, as an annotation processor).
     *
     * @param code   the return code
     */
    public static void exit(int code) {
	    System.err.println("Exit code: " + code);
    }
}
