package org.clyze.doop.jcplugin.util;

import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.code.Type.*;
import org.clyze.persistent.metadata.Printer;

import static org.clyze.doop.jcplugin.TypeInfoPlugin.context;

/**
 * This code erases types.
 */
public class TypeTransformer {

    /**
     * Erase a type t. If context is not null, we use it to call the erasure
     * code of javac. As a fallback (e.g., for handling API changes),
     * if context is null, we use a naive built-in method that mostly works.
     *
     * @param p    the output printer to use
     * @param t    the type to erase
     * @return     the erased type as a String
     */
    public static String eraseGenericType(Printer p, Type t) {
        if (context != null) {
            if (t instanceof Type.ArrayType) {
                Type elemType = ((Type.ArrayType)t).getComponentType();
                return eraseGenericType(p, elemType) + "[]";
            } else {
                return eraseTypeToFlatName(p, t);
            }
        } else {
            p.println("WARNING: using buggy eraseGenericType().");
            return eraseGenericTypeWithoutContext(t);
        }
    }

    static boolean eraseTypeToFlatName_heuristic = false;

    /**
     * Erase a Java type. This method tries to use functionality
     * provided by the javac API. If this attempt fails, it uses
     * a workaround.
     *
     * @param t    the type to erase
     * @return     the erased type as a String
     */
    private static String eraseTypeToFlatName(Printer p, Type t) {
        try {
            return Types.instance(context).erasure(t).tsym.flatName().toString();
        } catch (Throwable tr) {
            if (!eraseTypeToFlatName_heuristic) {
                eraseTypeToFlatName_heuristic = true;
                p.println("eraseTypeToFlatName(Type t) does not work, using own heuristic.");
            }
            return eraseGenericTypeWithoutContext(t);
        }
    }

    /** Erase a type t without using the javac API. This mostly works
     *  but has at least two bugs:
     *
     * - Erasure of T[], when T is a type variable, should be
     *   java.lang.Object[].
     *
     * - Erasure of generic methods (e.g, '<T extends C> T meth(T x)')
     *   forgets the bound C and produces java.lang.Object.
     *
     * @param t    the type to erase
     * @return     the erased type as a String
     */
    private static String eraseGenericTypeWithoutContext(Type t) {
        if (t instanceof TypeVar) {
            return "java.lang.Object";
        } else if (t instanceof WildcardType) {
            // Wildcards need more logic to erase, they must be
            // replaced by their bound.
            throw new RuntimeException("Wildcard type erasure is not yet supported");
        } else if (t instanceof ForAll) {
            // The Javadoc says about ForAll types: "Kind of
            // type-constraint derived during type inference". Thus
            // they should never appear in type-checked sources.
            throw new RuntimeException("For-all type erasure is not yet supported");
        }
        return eraseGenericTypeString(t.toString());
    }

    private static String eraseGenericTypeString(String genericType) {
        int count = 0;
        while (genericType.contains("<")) {
            genericType = genericType.replaceAll("<(\\w|,|\\.|\\$|\\?|\\s|\\[|\\])*>", "");
            if (count++ > 100)
                throw new RuntimeException("Cannot erase type or type too deep: " + genericType);
        }
        return genericType;
    }

    public static String translateEnclosingToDollar(Printer p, Type t) {
        String name = eraseTypeToFlatName(p, t);
        char[] chars = name.toCharArray();
        Type enclosingType;
        while ((enclosingType = t.getEnclosingType()) != null) {
            String enclosingName = eraseTypeToFlatName(p, enclosingType);
            p.println("name = " + name + ", enclosingType = " + enclosingType);
            if (name.startsWith(enclosingName))
                chars[enclosingName.length() + 1] = '$';
            t = enclosingType;
        }
        return new String(chars);
    }

    /**
     * Translates a nested type to use a dollar (A.B to A$B). This is
     * a heuristic and will fail for fully qualified names with
     * packages starting with an uppercase letter.
     *
     * @param s   the type representation as a String
     * @return    the type representation with dollars for nested types
     */
    public static String translateDotToDollar(String s) {
        char[] chars = s.toCharArray();
        boolean classFound = Character.isUpperCase(s.charAt(0));
        for (int i = 1; i < chars.length - 1; i++) {
            if ((chars[i] == '.') && Character.isUpperCase(chars[i+1])) {
                if (!classFound)
                    classFound = true;
                else
                    chars[i] = '$';
            }
        }
        return new String(chars);
    }

    public static String eraseAndTranslateType(Printer p, Type t) {
        return translateDotToDollar(eraseGenericType(p, t));
    }
}
