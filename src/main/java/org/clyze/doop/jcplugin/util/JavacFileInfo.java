package org.clyze.doop.jcplugin.util;

import com.sun.source.tree.CompilationUnitTree;
import com.sun.source.tree.LineMap;
import com.sun.tools.javac.util.Position;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.clyze.persistent.metadata.FileInfo;
import org.clyze.persistent.metadata.jvm.JvmMetadata;

import static org.apache.commons.io.FilenameUtils.getName;

public class JavacFileInfo extends FileInfo {

    /** Javac line map for the compilation unit, to retrieve line and column numbers from character offsets */
    private LineMap lineMap;

    public JavacFileInfo(JavaFileObject file, CompilationUnitTree compUnit, String inputFilePath, String source) {
        super((compUnit.getPackageName() != null ? compUnit.getPackageName() + "." : ""),
                getName(inputFilePath),
                getFilePath(file),
                source,
                new JvmMetadata());
    }

    public JavacFileInfo(JavaFileObject file, CompilationUnitTree compUnit) throws IOException {
        this(file, compUnit, getFilePath(file), compUnit.getSourceFile().getCharContent(true).toString());

        /*
         * The line map provided by the compilation unit (i.e. "compUnit.getLineMap()")
         * calculates the positions with the "tabs expanded" option set to "true", which
         * in practice results to treating a tab character as 1-8 space characters.
         *
         * Since we want a tab to be counted as one character we must get/create the line map
         * in the following way:
         */
        lineMap = Position.makeLineMap(source.toCharArray(), source.length(), false);
    }


    /**
     * Returns the line at a given offset.
     *
     * @param offset    the offset to use for searching the line
     * @return          the line number in source code starting from "1"
     */
    public long getLineNumber(long offset) {
        return (offset < 0) ? 0 : lineMap.getLineNumber(offset);
    }

    /**
     * Returns the column at a given offset.
     *
     * @param offset    the offset to use for searching the column
     * @return          te column number on source code starting from "1"
     */
    public long getColumnNumber(long offset) {
        return (offset < 0) ? 0 : lineMap.getColumnNumber(offset);
    }

    protected void printPatternError(String regex, long offset) {
        System.err.println("Unable to find pattern: \"" + regex + "\"" +
                " @ " + inputFilePath +
                " [" + getLineNumber(offset) + "," + getColumnNumber(offset) + "]");
    }

    public static String getFilePath(JavaFileObject file) {
        return file.getName();
    }

    /**
     * Searches for a string in the text.
     *
     * @param text      string to search for in the source code
     * @param offset    character offset in the source code to start searching from
     * @return          "true" if the given text existed at the given offset; otherwise "false"
     */
    public boolean existsInSource(String text, long offset) {
        return text.regionMatches(0, source, (int)offset, text.length());
    }

    /**
     * Counts the number of whitespace characters.
     *
     * @param offset    character offset in the source code to start counting whitespaces
     * @return          the character offset of the first non-whitespace character after "offset-1"
     */
    public long countWhitespaces(long offset) {
        return countRegexLength(offset, "\\A(\\s*)");
    }

    /**
     * Counts the length of the method prefix.
     *
     * @param offset    character offset in the source code where the method reference prefix (e.g. java.lang.String::) begins
     * @return          the character offset where the referenced method name begins
     */
    public long countMethodRefPrefix(long offset) {
        return countRegexLength(offset, "\\A([^:]*::[\\s]*)");
    }

    /**
     * Applies a regex to the text.
     *
     * @param offset    character offset in the source code to apply the regex
     * @param regex     the ... regex string! ^^
     * @return          "offset" plus the length of the string that resolved the regex
     */
    public long countRegexLength(long offset, String regex) {
        /*
         * First, search only in the next few characters (e.g. 64), after "offset". In practice, this should be enough because:
         * 1) The regex will always match. That is because the source should have already been successfully parsed and this
         *    method's caller should search with a reasonable regex, context-wise.
         * 2) At the moment, the used regex-es do not describe code that would require more than a few characters to express,
         *    thus even 64 characters should be enough for the regex to match.
         *
         * If not matched, then search in more characters and finally search the whole source from "offset" until the end.
         */

        Pattern p = Pattern.compile(regex);
        Matcher m;
        int start = (int)offset;
        int[] ends = {
                Math.min(source.length(), start + 64),
                Math.min(source.length(), start + 512),
                source.length()
        };
        int prevEnd = -1;

        for(int end : ends) {
            if(prevEnd == end) {
                // Do not repeat the exact same search
                break;
            }
            prevEnd = end;

            // "CharBuffer.wrap" is used instead of "source.substring" for better performance.
            // "String::substring" is O(N) instead of O(1) since Java 7.

            m = p.matcher(CharBuffer.wrap(source, start, end));
            if(m.find()) {
                return offset + m.end() - m.start();
            }
        }

        // Normally, it should never reach here.
        printPatternError(regex, offset);

        return offset;
    }

    public org.clyze.persistent.model.Position getPosition(long beginPosition, long endPosition) {
        return new org.clyze.persistent.model.Position(
                getLineNumber(beginPosition),
                getLineNumber(endPosition),
                getColumnNumber(beginPosition),
                getColumnNumber(endPosition)
        );
    }

    /**
     * Get the position of a piece of text.
     *
     * @param offset    the offset in characters, where the element appears in the source
     * @param text      the text
     * @return          the element's position
     */
    public org.clyze.persistent.model.Position getPosition(long offset, String text) {
        return new org.clyze.persistent.model.Position(
                getLineNumber(offset),
                getColumnNumber(offset),
                getColumnNumber(offset) + text.length()
        );
    }


}
