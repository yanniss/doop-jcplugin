package org.clyze.doop.jcplugin.util;

import org.clyze.persistent.model.Position;

/**
 * A simple generator for usage element ids.
 */
public class UsageIdGen {
    public static String getUsageId(String filename, Position pos) {
        return "Usage-" + filename + "-" + pos.getEndLine() + "-" + pos.getEndLine() + "-" + pos.getStartColumn() + "-" + pos.getEndColumn();
    }
}
