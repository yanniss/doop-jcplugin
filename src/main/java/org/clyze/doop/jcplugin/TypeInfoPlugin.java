package org.clyze.doop.jcplugin;

import com.sun.source.util.JavacTask;
import com.sun.source.util.Plugin;
import com.sun.tools.javac.api.BasicJavacTask;
import com.sun.tools.javac.util.Context;
import java.io.File;
import org.clyze.persistent.metadata.Configuration;
import org.clyze.persistent.metadata.Printer;

/**
 *
 */
public class TypeInfoPlugin implements Plugin {

    public static Context context = null;

    @Override
    public String getName() {

        return "TypeInfoPlugin";
    }

    @Override
    public void init(JavacTask task, String... args) {

        if (task instanceof BasicJavacTask)
            context = ((BasicJavacTask)task).getContext();
        else
            System.err.println("WARNING: JavacTask not fully supported.");

        Printer printer = new Printer(args != null && args.length > 1 && args[0].equals("true"));
        Configuration configuration = new Configuration(printer);
        if (args != null && args.length > 0) {
            String outDir = args[0];
            File outDirFile = new File(outDir);
            if (!outDirFile.exists())
                if (!outDirFile.mkdirs())
                    System.err.println("WARNING: directory already exists: " + outDir);
            configuration.setOutDir(outDirFile);
        }

        task.addTaskListener(new TypeInfoTaskListener(configuration));
    }

}
