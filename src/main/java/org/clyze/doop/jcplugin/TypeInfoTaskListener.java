package org.clyze.doop.jcplugin;

import com.sun.source.util.TaskEvent;
import com.sun.source.util.TaskListener;
import com.sun.tools.javac.tree.JCTree;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.clyze.doop.jcplugin.util.JavacFileInfo;
import org.clyze.doop.jcplugin.visitors.*;
import org.clyze.persistent.metadata.Configuration;
import org.clyze.persistent.metadata.FileReporter;

import static com.sun.source.util.TaskEvent.Kind.ANALYZE;

/**
 *
 */
public class TypeInfoTaskListener implements TaskListener {

    private static final Map<String, JavacFileInfo> filesInfo = new HashMap<>();
    private final Configuration configuration;

    public TypeInfoTaskListener(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param e     The started task event
     */
    @Override
    public void started(TaskEvent e) {

    }

    /**
     * At the end of the ANALYZE task for a compilation unit, scan the AST and report found elements.
     *
     * @param e     The finished task event
     */
    @Override
    public void finished(TaskEvent e) {

        TaskEvent.Kind kind = e.getKind();

        if(kind == ANALYZE) {

            configuration.printer.println(
                "\033[30m===================================================================================\033[0m\n" +
                "[\033[34m" + kind + "\033[0m " +
                "\033[31mfinished\033[0m] " +
                "@file:\033[35m" + e.getSourceFile().getName() + "\033[0m " +
                "@elem:\033[36m" + e.getTypeElement() + "\033[0m"
            );

            try {
                // Get a FileInfo object for this compilation unit
                String key = JavacFileInfo.getFilePath(e.getSourceFile());
                JavacFileInfo fInfo = filesInfo.get(key);
                if(fInfo == null) {
                    fInfo = new JavacFileInfo(e.getSourceFile(), e.getCompilationUnit());
                    filesInfo.put(key, fInfo);
                }

                // Get the AST root for this compilation unit
                JCTree treeRoot = (JCTree)e.getCompilationUnit();

                // Scan the AST to collect the elements
                treeRoot.accept(new ElementScanner(fInfo, configuration));

                FileReporter reporter = new FileReporter(configuration, fInfo.getElements());
                reporter.printReportStats();
                reporter.createReportFile(fInfo.getOutputFilePath());
            }
            catch(IOException ex) {
                ex.printStackTrace();
                Main.exit(-1);
            }
        }
    }

}
