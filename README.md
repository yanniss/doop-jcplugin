# Doop Plugin for the Java Compiler #
This Java compiler plugin scans the source code of Java programs in order to scavenge all static type information, including classes, methods, variables, fields, occurrences of variables and fields (writes and reads), methods, method invocations and heap allocations among others. The Doop plugin stores the scavenged information in JSON files based on the specification provided by the doop-persistent project. Part of the generated information is Doop-specific, but most of it is universal.

### Create JAR with dependencies ###

```
./gradlew fatJar
```

### A test use case ###

This plugin offers an invocation script that can be used as a replacement for javac. The invocation script
accepts identical arguments with javac, while enabling the plugin automagically. It also supports the additional `-sd`
flag to indicate the directory to place the JSON files. 

For example, clone the doop-jcplugin project and a test project:

        git clone git@bitbucket.org:yanniss/doop-jcplugin.git
        git clone git@github.com:plast-lab/viewer-demo.git
        cd doop-jcplugin
        ./gradlew run -Pargs="-cp ../viewer-demo/src/main/java -d test -sd test/json ../viewer-demo/src/main/java/demo/Main.java"

                
Or, equivalently:

        ./gradlew installDist
        ./build/install/doop-jcplugin/bin/doop-jcplugin -cp ../viewer-demo/src/main/java -d test -sd test/json ../viewer-demo/src/main/java/demo/Main.java

### Restrictions - requirements ###
Currently, the Doop javac plugin requires Java JDK 8 to be installed in the system since it is the only version of the JDK supporting Java compiler plugins.

### Useful links ###
Javac sources:
http://www.docjar.com/docs/api/com/sun/tools/javac/tree/JCTree.html
http://cr.openjdk.java.net/~forax/lambda/src/share/classes/com/sun/tools/javac/tree/JCTree.java.html

During development you should have JVM assertions enabled (VM option: "-ea").
